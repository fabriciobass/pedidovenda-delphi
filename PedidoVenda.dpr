program PedidoVenda;

uses
  Vcl.Forms,
  uPedido in 'class\uPedido.pas',
  uDados in 'models\uDados.pas' {uDM: TDataModule},
  uClientes in 'class\uClientes.pas',
  uPrincipal in 'view\uPrincipal.pas' {frmPrincipal},
  uPesqCliente in 'view\uPesqCliente.pas' {frmPesqCliente},
  uPesqProduto in 'view\uPesqProduto.pas' {frmPesqProduto},
  uProduto in 'class\uProduto.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TuDM, uDM);
  Application.CreateForm(TfrmPrincipal, frmPrincipal);
  Application.Run;
end.
