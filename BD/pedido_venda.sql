-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 30-Jul-2021 às 20:15
-- Versão do servidor: 10.4.20-MariaDB
-- versão do PHP: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `pedido_venda`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE `cliente` (
  `codigo` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `cidade` varchar(50) NOT NULL,
  `uf` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`codigo`, `nome`, `cidade`, `uf`) VALUES
(1, 'FABRICIO RODRIGUES', 'BELÉM', 'PA'),
(2, 'DÉBORA COSTA', 'SÃO PAULO', 'SP'),
(3, 'REBECA CARVALHO', 'PORTEL', 'MG'),
(4, 'MARIA RITA', 'RIO DE JANEIRO', 'RJ'),
(5, 'JOANA SANTOS', 'BELÉM', 'PA'),
(6, 'JULIA BRITO', 'RIO DE JANEIRO', 'RJ'),
(7, 'MARIA LUCIA', 'RIO DE JANEIRO', 'RJ'),
(8, 'JOÃO VITOR', 'RIO DE JANEIRO', 'RJ'),
(9, 'FELIPE SOARES', 'RIO DE JANEIRO', 'RJ'),
(10, 'EMERSON CARDOSO', 'RIO DE JANEIRO', 'RJ'),
(11, 'EDIEL ROCHA', 'RIO DE JANEIRO', 'RJ'),
(12, 'CLEY DUARTE', 'RIO DE JANEIRO', 'RJ'),
(13, 'CALEBE BASS', 'RIO DE JANEIRO', 'RJ'),
(14, 'DIOGENES MELO', 'RIO DE JANEIRO', 'RJ'),
(15, 'ADELSON MOURA', 'RIO DE JANEIRO', 'RJ'),
(16, 'FLAVIA RODRIGUES', 'RIO DE JANEIRO', 'RJ'),
(17, 'NATAN SANTOS', 'RIO DE JANEIRO', 'RJ'),
(18, 'SOFIA BRAGA', 'RIO DE JANEIRO', 'RJ'),
(19, 'CARLOS ARAGÃO', 'RIO DE JANEIRO', 'RJ'),
(20, 'MARLUCE FERNANDES', 'RIO DE JANEIRO', 'RJ'),
(21, 'LUZIA BRAGA', 'RIO DE JANEIRO', 'RJ'),
(22, 'MARIA DO SOCORRO', 'RIO DE JANEIRO', 'RJ');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido`
--

CREATE TABLE `pedido` (
  `num_pedido` int(11) NOT NULL,
  `data_emissao` date NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `valor_total` decimal(15,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Acionadores `pedido`
--
DELIMITER $$
CREATE TRIGGER `Tgr_Pedido_Delete` BEFORE DELETE ON `pedido` FOR EACH ROW BEGIN
	DELETE FROM pedido_itens WHERE num_pedido = OLD.num_pedido;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_itens`
--

CREATE TABLE `pedido_itens` (
  `codigo` int(11) NOT NULL,
  `num_pedido` int(11) NOT NULL,
  `cod_produto` int(11) NOT NULL,
  `quantidade` decimal(15,2) NOT NULL,
  `vl_unitario` decimal(15,2) NOT NULL,
  `vl_total` decimal(15,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE `produto` (
  `codigo` int(11) NOT NULL,
  `descricao` varchar(100) NOT NULL,
  `preco` decimal(15,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`codigo`, `descricao`, `preco`) VALUES
(1, 'FEIJÃO FD 30 KG', '50.00'),
(2, 'ARROZ 30KG', '15.00'),
(4, 'MARACARRAO 30G', '20.00'),
(5, 'ARROZ 30KG', '15.00'),
(6, 'MACARRAO 30KG', '22.00'),
(7, 'AÇUCAR 30KG', '152.00'),
(8, 'MILHO VERDE', '125.00'),
(9, 'OLEO', '155.00'),
(10, 'VERDURA KG', '15.00'),
(11, 'MATEIGA LT', '223.00'),
(12, 'SARDINHA CX', '133.00'),
(13, 'CARNE MOIDA', '189.00'),
(14, 'BISCOITO CX', '145.00'),
(15, 'MORTADELA', '125.00'),
(16, 'FRANGO CX', '235.00'),
(17, 'CONVERVA CX', '285.00'),
(18, 'PIMENTA FD', '195.00'),
(19, 'COPO DESCARTAVEL', '105.00'),
(20, 'SAL FD', '165.00');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`codigo`);

--
-- Índices para tabela `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`num_pedido`),
  ADD KEY `pedido_id_cliente` (`id_cliente`);

--
-- Índices para tabela `pedido_itens`
--
ALTER TABLE `pedido_itens`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `pedidoItens_id_produto` (`cod_produto`),
  ADD KEY `pedidoItens_num_pedido` (`num_pedido`);

--
-- Índices para tabela `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`codigo`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `cliente`
--
ALTER TABLE `cliente`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de tabela `pedido`
--
ALTER TABLE `pedido`
  MODIFY `num_pedido` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `pedido_itens`
--
ALTER TABLE `pedido_itens`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `produto`
--
ALTER TABLE `produto`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `pedido`
--
ALTER TABLE `pedido`
  ADD CONSTRAINT `pedido_id_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`codigo`);

--
-- Limitadores para a tabela `pedido_itens`
--
ALTER TABLE `pedido_itens`
  ADD CONSTRAINT `pedidoItens_id_produto` FOREIGN KEY (`cod_produto`) REFERENCES `produto` (`codigo`),
  ADD CONSTRAINT `pedidoItens_num_pedido` FOREIGN KEY (`num_pedido`) REFERENCES `pedido` (`num_pedido`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
