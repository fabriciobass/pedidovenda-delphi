unit uProduto;

interface
  uses SysUtils;

  type
    TProdutos = class
    private
      FCodigo: Integer;
      FDescricao: string;
      FPreco: Currency;

    procedure SetCodigo(const Value: integer);
    procedure SetDescricao(const Value: string);
    procedure SetPreco(const Value: Currency);

    public
     property Codigo : integer read FCodigo write SetCodigo;
     property Descricao : string read FDescricao write SetDescricao;
     property Preco : Currency read FPreco write SetPreco;

     function Selecionar(Codigo: Integer; Descricao, Ordem: String):Boolean;
     function Inserir : boolean;
     function Alterar : boolean;
     function Deletar(codigo: integer) : boolean;
    end;

implementation

uses uDados;

function TProdutos.Alterar: boolean;
begin
  with uDM.qryCliente do
  begin
    Close;
    SQL.Text := 'Update Produto Set descricao = :Descricao, Preco = :Preco where codigo = :Codigo';
    ParamByName('Codigo').Value    := FCodigo;
    ParamByName('Descricao').Value := FDescricao;
    ParamByName('Preco').Value     := FPreco;
    try
      ExecSQL;
      Result := true;
    except
      Result := False;
     end;
  end;
end;

function TProdutos.Deletar(Codigo: integer): boolean;
begin
  with uDM.qryCliente do
  begin
    Close;
    SQL.Text  := ' delete from Produto where codigo = :Codigo';
    ParamByName('Codigo').Value := Codigo;
    try
      ExecSQL;
      Result := True;
    except
      Result := False;
    end;
  end;
end;

function TProdutos.Inserir: boolean;
begin
  with uDM.qryCliente do
  begin
    Close;
    Sql.text := 'Insert into Produto (descricao, preco) ' +
             ' Values (:Descricao, :Preco)';
    ParamByName('Descricao').Value  := FDescricao;
    ParamByName('Preco').Value    := FPreco;
    try
      ExecSQL;
      result := true;
    except
      result := false;
    end;
  end;
end;

function TProdutos.Selecionar(Codigo: Integer; Descricao, Ordem: String): Boolean;
begin
  Descricao := '%'+Descricao+'%';
  with uDM.qryProduto do
  begin
    Close;
    Sql.Text := ' Select * from Produto where 1=1 ';

    if Codigo > 0 then
    begin
      Sql.add(' and codigo = :Codigo');
      ParamByName('Codigo').Value := Codigo;
     end;

    if Descricao <> '' then
    sql.add(' and descricao like '+quotedstr(Descricao));

    if Ordem <> '' then       sql.add(' Order by '+Ordem);
    try
      Open;
      if not eof then
        Result := true
      else
       Result := false;
    except
      Result := false;
    end;
  end;
end;

procedure TProdutos.SetCodigo(const Value: integer);
begin
  FCodigo := Value;
end;

procedure TProdutos.SetDescricao(const Value: string);
begin
  FDescricao := Value;
end;

procedure TProdutos.SetPreco(const Value: Currency);
begin
  FPreco := Value;
end;

end.
