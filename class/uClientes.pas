unit uClientes;

interface
  uses SysUtils;

  type
    TClientes = class
    private
      FCodigo: Integer;
      FNome: string;
      FCidade: string;
      FUF: string;

      procedure SetCodigo(const Value: integer);
      procedure SetNome(const Value: string);
      procedure SetCidade(const Value: string);
      procedure SetUF(const Value: string);

    public
     property Codigo : integer read FCodigo write SetCodigo;
     property Nome : string read FNome write SetNome;
     property Cidade : string read FCidade write SetCidade;
     property UF : string read FUF write SetUF;

     // M�todos
     function Selecionar(Codigo: Integer; Nome, Ordem: String):Boolean;
     function Inserir : boolean;
     function Alterar : boolean;
     function Deletar(codigo: integer) : boolean;
    end;

implementation { TClientes }

uses uDados;

function TClientes.Alterar: boolean;
begin
  with uDM.qryCliente do
  begin
    Close;
    SQL.Text := 'Update Cliente Set nome = :Nome, cidade = :Cidade, uf = :UF where codigo = :Codigo';
    ParamByName('Codigo').Value    := FCodigo;
    ParamByName('Nome').Value      := Fnome;
    ParamByName('Cidade').Value    := FCIdade;
    ParamByName('UF').Value        := FUF;
    try
      ExecSQL;
      Result := true;
    except
      Result := False;
     end;
  end;
end;

function TClientes.Deletar(Codigo: integer): boolean;
begin
  with uDM.qryCliente do
  begin
    Close;
    SQL.Text  := ' delete from Cliente where codigo = :Codigo';
    ParamByName('Codigo').Value := Codigo;
    try
      ExecSQL;
      Result := True;
    except
      Result := False;
    end;
  end;
end;

function TClientes.Inserir: boolean;
begin
  with uDM.qryCliente do
  begin
    Close;
    Sql.text := 'Insert into Cliente (nome, cidade, uf) ' +
             ' Values (:Nome, :Cidade, :uf)';
    ParamByName('Nome').Value      := FNome;
    ParamByName('Cidade').Value    := FCidade;
    ParamByName('uf').Value        := FUF;
    try
      ExecSQL;
      result := true;
    except
      result := false;
    end;
  end;
end;

function TClientes.Selecionar(Codigo: Integer; Nome, Ordem: String): Boolean;
begin
  Nome := '%'+Nome+'%';
  with uDM.qryCliente do
  begin
    Close;
    Sql.Text := ' Select * from Cliente where 1=1 ';

    if Codigo > 0 then
    begin
      Sql.add(' and codigo = :Codigo');
      ParamByName('Codigo').Value := Codigo;
     end;

    if Nome <> '' then
    sql.add(' and nome like '+quotedstr(Nome));

    if Ordem <> '' then       sql.add(' Order by '+Ordem);
    try
      Open;
      if not eof then
        Result := true
      else
       Result := false;
    except
      Result := false;
    end;
  end;
end;

procedure TClientes.SetCodigo(const Value: integer);
begin
  FCodigo := Value;
end;

procedure TClientes.SetCidade(const Value: string);
begin
  FCidade := Value;
end;

procedure TClientes.SetNome(const Value: string);
begin
  FNome := Value;
end;

procedure TClientes.SetUF(const Value: string);
begin
  FUF := Value;
end;

end.
