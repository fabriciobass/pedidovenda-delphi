object frmPrincipal: TfrmPrincipal
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'PEDIDO DE VENDA'
  ClientHeight = 635
  ClientWidth = 989
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    AlignWithMargins = True
    Left = 3
    Top = 232
    Width = 983
    Height = 343
    Align = alClient
    DataSource = dsPedidoItens
    DrawingStyle = gdsGradient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    ParentFont = False
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnKeyDown = DBGrid1KeyDown
    OnKeyPress = DBGrid1KeyPress
    Columns = <
      item
        Expanded = False
        FieldName = 'cod_produto'
        Title.Caption = 'C'#243'd. Prod.'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'descricao'
        Title.Caption = 'Nome Produto'
        Width = 400
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'quantidade'
        Title.Caption = 'QTD'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'vl_unitario'
        Title.Caption = 'Vlr. Unit'#225'rio'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'vl_total'
        Title.Caption = 'Vlr. Total'
        Visible = True
      end>
  end
  object GroupBox1: TGroupBox
    AlignWithMargins = True
    Left = 3
    Top = 44
    Width = 983
    Height = 73
    Align = alTop
    Caption = ' Dados do CLiente << F2 para pesquisar Cliente pelo Nome >>'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    ExplicitLeft = 0
    ExplicitTop = 0
    ExplicitWidth = 979
    object Label1: TLabel
      Left = 16
      Top = 17
      Width = 73
      Height = 17
      Caption = 'C'#243'd. Cliente:'
    end
    object Label2: TLabel
      Left = 168
      Top = 14
      Width = 82
      Height = 17
      Caption = 'Nome Cliente:'
    end
    object Label3: TLabel
      Left = 647
      Top = 14
      Width = 44
      Height = 17
      Caption = 'Cidade:'
    end
    object Label4: TLabel
      Left = 825
      Top = 14
      Width = 18
      Height = 17
      Caption = 'UF:'
    end
    object edtCodCliente: TEdit
      Left = 16
      Top = 34
      Width = 105
      Height = 25
      TabOrder = 0
      OnExit = edtCodClienteExit
      OnKeyPress = edtCodClienteKeyPress
    end
    object edtNome: TEdit
      Left = 168
      Top = 34
      Width = 473
      Height = 23
      TabStop = False
      Color = clSilver
      Ctl3D = False
      Enabled = False
      ParentCtl3D = False
      TabOrder = 1
    end
    object edtCidade: TEdit
      Left = 647
      Top = 34
      Width = 172
      Height = 23
      TabStop = False
      Color = clSilver
      Ctl3D = False
      Enabled = False
      ParentCtl3D = False
      TabOrder = 2
    end
    object edtUF: TEdit
      Left = 825
      Top = 34
      Width = 119
      Height = 23
      TabStop = False
      Color = clSilver
      Ctl3D = False
      Enabled = False
      ParentCtl3D = False
      TabOrder = 3
    end
    object btnPesqCliente: TButton
      Left = 127
      Top = 33
      Width = 35
      Height = 27
      Caption = 'F2'
      TabOrder = 4
      OnClick = btnPesqClienteClick
    end
  end
  object GroupBox2: TGroupBox
    AlignWithMargins = True
    Left = 3
    Top = 123
    Width = 983
    Height = 103
    Align = alTop
    Caption = ' Adicionar Produto  <<Apertar Enter para localizar o produto >> '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    ExplicitLeft = 0
    ExplicitTop = 114
    ExplicitWidth = 989
    object Label5: TLabel
      Left = 16
      Top = 18
      Width = 81
      Height = 17
      Caption = 'C'#243'd. Produto:'
    end
    object Label6: TLabel
      Left = 168
      Top = 18
      Width = 82
      Height = 17
      Caption = 'Nome Cliente:'
    end
    object Label7: TLabel
      Left = 647
      Top = 18
      Width = 71
      Height = 17
      Caption = 'Quantidade:'
    end
    object label20: TLabel
      Left = 728
      Top = 18
      Width = 83
      Height = 17
      Caption = 'Valor Unit'#225'rio:'
    end
    object Label8: TLabel
      Left = 825
      Top = 18
      Width = 31
      Height = 17
      Caption = 'Total:'
    end
    object edtCodProd: TEdit
      Left = 16
      Top = 37
      Width = 105
      Height = 25
      TabOrder = 0
      OnKeyPress = edtCodProdKeyPress
    end
    object edtNomeProd: TEdit
      Left = 168
      Top = 37
      Width = 473
      Height = 23
      Color = clSilver
      Ctl3D = False
      Enabled = False
      ParentCtl3D = False
      TabOrder = 1
    end
    object edtQtd: TEdit
      Left = 647
      Top = 37
      Width = 71
      Height = 25
      TabOrder = 2
      OnExit = edtQtdExit
      OnKeyPress = edtQtdKeyPress
    end
    object edtVlUnitario: TEdit
      Left = 728
      Top = 36
      Width = 91
      Height = 25
      TabOrder = 3
      OnExit = edtVlUnitarioExit
      OnKeyPress = edtVlUnitarioKeyPress
    end
    object edtTotal: TEdit
      Left = 825
      Top = 37
      Width = 119
      Height = 23
      Color = clSilver
      Ctl3D = False
      Enabled = False
      ParentCtl3D = False
      TabOrder = 4
    end
    object btnIncluir: TButton
      Left = 825
      Top = 67
      Width = 119
      Height = 31
      Caption = 'Incluir Produto'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btnIncluirClick
    end
    object btnPesqProduto: TButton
      Left = 127
      Top = 36
      Width = 35
      Height = 27
      Caption = 'F3'
      TabOrder = 6
      OnClick = btnPesqProdutoClick
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 578
    Width = 989
    Height = 57
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 496
    ExplicitWidth = 979
    object Label9: TLabel
      Left = 860
      Top = 6
      Width = 84
      Height = 16
      Caption = 'Total Pedido:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText1: TDBText
      Left = 860
      Top = 23
      Width = 119
      Height = 25
      DataField = 'TOTALPEDIDO'
      DataSource = dsPedidoItens
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btnGravarPedido: TButton
      Left = 7
      Top = 8
      Width = 115
      Height = 40
      Caption = 'Gravar Pedido (F5)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = btnGravarPedidoClick
    end
    object btnBucarPedido: TButton
      Left = 128
      Top = 8
      Width = 115
      Height = 40
      Caption = 'Buscar Pedido'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btnBucarPedidoClick
    end
    object btnCancelar: TButton
      Left = 249
      Top = 8
      Width = 115
      Height = 40
      Caption = 'Cancelar Pedido'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btnCancelarClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 989
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    Color = clBlue
    ParentBackground = False
    TabOrder = 4
    ExplicitLeft = 8
    ExplicitTop = -13
    ExplicitWidth = 979
    object Label10: TLabel
      Left = 11
      Top = 9
      Width = 88
      Height = 19
      Caption = 'Pedido N'#186':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txtNumPedido: TLabel
      Left = 104
      Top = 9
      Width = 40
      Height = 19
      Caption = '0000'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel
      Left = 763
      Top = 9
      Width = 116
      Height = 19
      Caption = 'Data Emiss'#227'o:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txtDataEmissao: TLabel
      Left = 887
      Top = 9
      Width = 5
      Height = 19
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
  end
  object dsPedidoItens: TDataSource
    DataSet = qryPedidoItens
    Left = 176
    Top = 200
  end
  object qryPedido: TFDQuery
    Connection = uDM.Conexao
    SQL.Strings = (
      'select * from pedido where num_pedido = :num_pedido')
    Left = 872
    Top = 32
    ParamData = <
      item
        Name = 'NUM_PEDIDO'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object qryPedidoItens: TFDQuery
    IndexFieldNames = 'num_pedido'
    AggregatesActive = True
    MasterSource = dsPedido
    MasterFields = 'num_pedido'
    Connection = uDM.Conexao
    SQL.Strings = (
      'select pi.*, p.descricao, p.codigo as cod_prod'
      'from pedido_itens pi '
      'left join produto p on p.codigo = pi.cod_produto'
      'where num_pedido = :num_pedido')
    Left = 560
    Top = 312
    ParamData = <
      item
        Name = 'NUM_PEDIDO'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object qryPedidoItenscodigo: TFDAutoIncField
      FieldName = 'codigo'
      Origin = 'codigo'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object qryPedidoItensnum_pedido: TIntegerField
      FieldName = 'num_pedido'
      Origin = 'num_pedido'
      Required = True
    end
    object qryPedidoItenscod_produto: TIntegerField
      FieldName = 'cod_produto'
      Origin = 'cod_produto'
      Required = True
    end
    object qryPedidoItensquantidade: TBCDField
      FieldName = 'quantidade'
      Origin = 'quantidade'
      Required = True
      currency = True
      Precision = 15
      Size = 2
    end
    object qryPedidoItensvl_unitario: TBCDField
      FieldName = 'vl_unitario'
      Origin = 'vl_unitario'
      Required = True
      currency = True
      Precision = 15
      Size = 2
    end
    object qryPedidoItensvl_total: TBCDField
      FieldName = 'vl_total'
      Origin = 'vl_total'
      Required = True
      currency = True
      Precision = 15
      Size = 2
    end
    object qryPedidoItensdescricao: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'descricao'
      Origin = 'descricao'
      ProviderFlags = []
      ReadOnly = True
      Size = 100
    end
    object qryPedidoItenscod_prod: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'cod_prod'
      Origin = 'codigo'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryPedidoItensTOTALPEDIDO: TAggregateField
      FieldName = 'TOTALPEDIDO'
      Active = True
      currency = True
      DisplayName = ''
      Expression = 'SUM(VL_TOTAL)'
    end
  end
  object dsPedido: TDataSource
    DataSet = qryPedido
    Left = 880
    Top = 96
  end
  object qryExecute: TFDQuery
    BeforeExecute = qryExecuteBeforeExecute
    Connection = uDM.Conexao
    Transaction = uDM.Transacao
    Left = 616
    Top = 248
  end
  object qryConsulta: TFDQuery
    BeforeExecute = qryExecuteBeforeExecute
    Connection = uDM.Conexao
    Transaction = uDM.Transacao
    Left = 472
    Top = 304
  end
end
