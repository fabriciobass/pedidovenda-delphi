object frmPesqProduto: TfrmPesqProduto
  Left = 0
  Top = 0
  Caption = 'Pesquisar Produto'
  ClientHeight = 367
  ClientWidth = 657
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 10
    Width = 145
    Height = 13
    Caption = 'Digite a Descri'#231#227'o do Produto:'
  end
  object edtPesqNome: TEdit
    Left = 8
    Top = 27
    Width = 561
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 0
    OnKeyDown = edtPesqNomeKeyDown
  end
  object DBGrid1: TDBGrid
    Left = 4
    Top = 54
    Width = 642
    Height = 291
    DataSource = dsPesqProduto
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    OnKeyPress = DBGrid1KeyPress
    Columns = <
      item
        Expanded = False
        FieldName = 'codigo'
        Title.Caption = 'C'#243'digo'
        Width = 45
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'descricao'
        Title.Caption = 'Descri'#231#227'o'
        Width = 450
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'preco'
        Title.Caption = 'Pre'#231'o'
        Visible = True
      end>
  end
  object btnPesquisar: TButton
    Left = 575
    Top = 25
    Width = 71
    Height = 25
    Caption = 'Pesquisar'
    TabOrder = 1
    OnClick = btnPesquisarClick
  end
  object qryPesqProduto: TFDQuery
    Connection = uDM.Conexao
    SQL.Strings = (
      'Select codigo, descricao, preco from produto')
    Left = 480
    Top = 184
    object qryPesqProdutocodigo: TFDAutoIncField
      FieldName = 'codigo'
      Origin = 'codigo'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object qryPesqProdutodescricao: TStringField
      FieldName = 'descricao'
      Origin = 'descricao'
      Required = True
      Size = 100
    end
    object qryPesqProdutopreco: TBCDField
      FieldName = 'preco'
      Origin = 'preco'
      Required = True
      currency = True
      Precision = 15
      Size = 2
    end
  end
  object dsPesqProduto: TDataSource
    DataSet = qryPesqProduto
    Left = 176
    Top = 200
  end
end
