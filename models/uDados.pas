unit uDados;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.MySQL,
  FireDAC.Phys.MySQLDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, Vcl.Forms, IniFiles, Dialogs;

type
  TuDM = class(TDataModule)
    Conexao: TFDConnection;
    FBDriver: TFDPhysMySQLDriverLink;
    qryCliente: TFDQuery;
    Transacao: TFDTransaction;
    qryNumerador: TFDQuery;
    qryProduto: TFDQuery;
    qryProdutocodigo: TFDAutoIncField;
    qryProdutodescricao: TStringField;
    qryProdutopreco: TBCDField;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    function Numerador(Tabela, Campo, filtra, where, Valor: String): Integer;
  end;

var
  uDM: TuDM;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}



{ TuDM }

procedure TuDM.DataModuleCreate(Sender: TObject);
var
  iArq: TIniFile;
  nTentativas: word;
begin

  try

    nTentativas := 1;
    iArq := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'Banco.ini');

    Conexao.Params.Values['DriverID'] := 'MySQL';
    Conexao.Params.Values['Server'] := iArq.ReadString('BD', 'IP', '');
    Conexao.Params.Values['Database'] := iArq.ReadString('BD', 'DATA', '');
    Conexao.Params.Values['Password'] := iArq.ReadString('BD', 'PASS', '');
    Conexao.Params.Values['User_name'] := iArq.ReadString('BD', 'USER', '');
    FBDriver.VendorLib := ExtractFilePath(Application.ExeName) + 'libmysql.dll';

    try
      Conexao.Connected := true;
    Except
      ShowMessage('N�o foi possivel conectar na base de dados!');
      Application.Terminate;
    end;

  Finally
    iArq.Free;
  end;
end;

function TuDM.Numerador(Tabela, Campo, filtra, where, Valor: String): Integer;
begin
  Result := 0;
  if filtra = 'N' then
  begin
    qryNumerador.Close;

    qryNumerador.sql.Text := 'SELECT MAX(' + Campo + ')MAIOR FROM ' + Tabela;
    qryNumerador.Open;
  end;
  if filtra = 'S' then
  begin
    qryNumerador.Close;
    qryNumerador.sql.Text := 'SELECT MAX(' + Campo + ')MAIOR FROM ' + Tabela +
      ' WHERE ' + where + '=' + Valor;
    qryNumerador.Open;
  end;
  Result := qryNumerador.Fields[0].AsInteger + 1;
end;

end.
