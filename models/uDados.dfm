object uDM: TuDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 484
  Width = 660
  object Conexao: TFDConnection
    Params.Strings = (
      'Database=pedido_venda'
      'User_Name=root'
      'DriverID=MySQL')
    LoginPrompt = False
    Transaction = Transacao
    Left = 48
    Top = 16
  end
  object FBDriver: TFDPhysMySQLDriverLink
    Left = 48
    Top = 80
  end
  object qryCliente: TFDQuery
    Connection = Conexao
    Left = 128
    Top = 16
  end
  object Transacao: TFDTransaction
    Connection = Conexao
    Left = 48
    Top = 136
  end
  object qryNumerador: TFDQuery
    Connection = Conexao
    Left = 48
    Top = 200
  end
  object qryProduto: TFDQuery
    Connection = Conexao
    SQL.Strings = (
      'select codigo, descricao, preco from produto')
    Left = 128
    Top = 72
    object qryProdutocodigo: TFDAutoIncField
      FieldName = 'codigo'
      Origin = 'codigo'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object qryProdutodescricao: TStringField
      FieldName = 'descricao'
      Origin = 'descricao'
      Required = True
      Size = 100
    end
    object qryProdutopreco: TBCDField
      FieldName = 'preco'
      Origin = 'preco'
      Required = True
      Precision = 15
      Size = 2
    end
  end
end
